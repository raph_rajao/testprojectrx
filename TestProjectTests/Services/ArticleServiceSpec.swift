//
//  ArticleServiceSpec.swift
//  TestProjectTests
//
//  Created by Raphael Rajaofetra on 2020-11-23.
//

import Quick
import Nimble
import RxSwift
@testable import TestProject

class ArticleServiceSpec: QuickSpec {
    override func spec() {
        var network: Network!
        var disposeBag: DisposeBag!
        var service: ArticleService!
        
        beforeEach {
            network = Network()
            disposeBag = DisposeBag()
            service = ArticleService(network: network)
        }
        
        describe("article search") {
            it("eventually the list of articles") {
                var response: Response? = nil
                
                service.loadArticles()
                    .subscribe(onNext: {
                        response = $0
                    }).disposed(by: disposeBag)
                
                expect(response?.totalResults).toEventually(beGreaterThanOrEqualTo(1))
            }
        }
    }
}
