//
//  Networkspec.swift
//  TestProjectTests
//
//  Created by Raphael Rajaofetra on 2020-11-23.
//

import Quick
import Nimble
import RxSwift
import RxCocoa
import Himotoki
@testable import TestProject

class NetworkSpec: QuickSpec {
    override func spec() {
        var network: Network!
        var disposeBag: DisposeBag!
        
        beforeEach {
            network = Network()
            disposeBag = DisposeBag()
        }
        
        describe("image") {
            it("eventually return an image") {
                var image: UIImage? = nil
                let url = "https://editorial.fxstreet.com/images/Markets/Currencies/Digital%20Currencies/Bitcoin/bitcoin_4_Large.jpg"
                network.image(url: url)
                    .subscribe(onNext: { image = $0 })
                    .disposed(by: disposeBag)
                
                expect(image).toEventuallyNot(beNil())
            }
            
            it("eventually gets an error on non image URL") {
                    var error: NetworkError? = nil
                    let url = "https://httpbin.org/get"
                    network.image(url: url)
                        .subscribe(onError: { error = $0 as? NetworkError })
                        .disposed(by: disposeBag)
                    
                    expect(error).toEventually(equal(NetworkError.incorrectData))
            }
        }
        
        
    }
}

struct HttpBinResponse {
    let args: HttpBinArgs
}

extension HttpBinResponse: Decodable {
    static func decode(_ e: Extractor) throws -> HttpBinResponse {
        return try HttpBinResponse(
            args: e <| "args"
        )
    }
}

struct HttpBinArgs {
    let id: String
    let age: String
}

extension HttpBinArgs: Decodable {
    static func decode(_ e: Extractor) throws -> HttpBinArgs {
        return try HttpBinArgs(
            id: e <| "id",
            age: e <| "age"
        )
    }
}

