//
//  ViewController.swift
//  TestProject
//
//  Created by Raphael Rajaofetra on 2020-11-23.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {
    private let titleLabel = UILabel()
    var articleListViewModel:  ArticleListViewModeling!

    var collectionView: UICollectionView!
    private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.titleLabel.text = "Publication Récentes"
        self.view.addSubview(self.titleLabel)
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.titleLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.titleLabel.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 25).isActive = true

        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: self.view.frame.width, height: 400)
        layout.scrollDirection = .horizontal
        self.collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        self.collectionView.isPagingEnabled = true
        self.collectionView.alwaysBounceHorizontal = true
        self.collectionView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.collectionView)
        self.collectionView.backgroundColor = UIColor.white
        self.collectionView.register(ArticleCell.self, forCellWithReuseIdentifier: "ArticleCell")
        self.collectionView.topAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: 10).isActive = true
        self.collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -10).isActive = true
        self.collectionView.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true

        setupBindings()
    }
    
    private func setupBindings() {
        collectionView
            .rx.delegate
            .setForwardToDelegate(self, retainDelegate: false)
        articleListViewModel.cellModels.bind(to: collectionView.rx.items(cellIdentifier: "ArticleCell", cellType: ArticleCell.self)) {
            i, cellModel, cell in
            cell.viewModel = cellModel
        }.disposed(by: disposeBag)
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

