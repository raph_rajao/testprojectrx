//
//  ArticleCell.swift
//  HorizontalList
//
//  Created by Raphael Rajaofetra on 2020-11-23.
//

import UIKit
//import Spring
import RxSwift
import RxCocoa

class ArticleCell: UICollectionViewCell {
    private let titleLabel = UILabel()
    private let sourceLabel = UILabel()
    private let descriptionLabel = UILabel()
    private let imageView = UIImageView()
    
    private var disposeBag: DisposeBag? = DisposeBag()
    
    var viewModel: ArticleCellModeling? {
        didSet {
            let disposeBag = DisposeBag()
            guard let viewModel = viewModel else { return }
            
            viewModel.image
                .bind(to: imageView.rx.image)
                .disposed(by: disposeBag)
            
            titleLabel.text = viewModel.title
            sourceLabel.text = viewModel.source
            descriptionLabel.text = viewModel.description
            
            self.disposeBag = disposeBag
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        setupConstraints()
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)

        self.setupConstraints()
    }
    
    private func setupConstraints() {
        self.titleLabel.numberOfLines = 1
        self.descriptionLabel.numberOfLines = 2
        self.sourceLabel.textColor = UIColor.white
        self.contentView.addSubview(imageView)
        self.imageView.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(titleLabel)
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(sourceLabel)
        self.sourceLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(descriptionLabel)
        self.descriptionLabel.translatesAutoresizingMaskIntoConstraints = false

        self.titleLabel.rightAnchor.constraint(equalTo: self.contentView.rightAnchor).isActive = true
        self.titleLabel.leftAnchor.constraint(equalTo: self.contentView.leftAnchor).isActive = true
        
        self.sourceLabel.rightAnchor.constraint(equalTo: self.contentView.rightAnchor).isActive = true
        self.sourceLabel.leftAnchor.constraint(equalTo: self.contentView.leftAnchor).isActive = true
        
        self.descriptionLabel.rightAnchor.constraint(equalTo: self.contentView.rightAnchor).isActive = true
        self.descriptionLabel.leftAnchor.constraint(equalTo: self.contentView.leftAnchor).isActive = true


        self.imageView.layer.cornerRadius = 4
        self.imageView.backgroundColor = UIColor.red
        self.imageView.widthAnchor.constraint(equalTo: self.contentView.widthAnchor).isActive = true

        self.imageView.topAnchor.constraint(equalTo: self.contentView.topAnchor).isActive = true
        self.imageView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        self.sourceLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor).isActive = true
        
        self.titleLabel.topAnchor.constraint(equalTo: self.imageView.bottomAnchor, constant: 10).isActive = true
        self.descriptionLabel.topAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: 10).isActive = true
    }
}
