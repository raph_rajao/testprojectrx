//
//  ArticleListViewModel.swift
//  HorizontalList
//
//  Created by Raphael Rajaofetra on 2020-11-23.
//

import RxSwift
import RxCocoa

protocol ArticleListViewModeling {
    var cellModels: Observable<[ArticleCellModeling]> { get }
}

class ArticleListViewModel: ArticleListViewModeling {
    let cellModels: Observable<[ArticleCellModeling]>
    
    init(network: Networking,
         articleService: ArticleServicing) {
        
        self.cellModels = articleService.loadArticles().catchErrorJustReturn(Response(totalResults: 0, articles: [])).map { response in
            Array(response.articles.map { article in
                return ArticleCellModel(network: network, imageUrl: article.image ?? "", title: article.title, source: article.source ?? "", description: article.description)
            }.prefix(20))
        }
        }
    }


