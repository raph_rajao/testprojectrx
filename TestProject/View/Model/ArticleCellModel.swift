//
//  ArticleCellModel.swift
//  HorizontalList
//
//  Created by Raphael Rajaofetra on 2020-11-23.
//

import RxSwift
import RxCocoa

protocol ArticleCellModeling {
    var image: Observable<UIImage> { get }
    var title: String { get }
    var source: String { get }
    var description: String { get }
}

class ArticleCellModel: ArticleCellModeling {
    let image: Observable<UIImage>
    let title: String
    let source: String
    let description: String
    
    init(network: Networking, imageUrl: String, title: String, source: String, description: String) {
        let placeholder = UIImage()
        self.image = Observable.just(placeholder)
            .concat(network.image(url: imageUrl))
            .observeOn(MainScheduler.instance)
            .catchErrorJustReturn(placeholder)
        self.title = title
        self.source = source
        self.description = description
    }
}

