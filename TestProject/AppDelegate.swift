//
//  AppDelegate.swift
//  TestProject
//
//  Created by Raphael Rajaofetra on 2020-11-23.
//

import UIKit
import Swinject
import SwinjectStoryboard

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let container = Container()

        container.register(Networking.self) { _ in
            Network()
        }
        container.register(ArticleServicing.self) { r in
            ArticleService(network: r.resolve(Networking.self)!)
        }
        container.register(ArticleListViewModeling.self) { r in
            ArticleListViewModel(
                network: r.resolve(Networking.self)!,
                articleService: r.resolve(ArticleServicing.self)!)
        }

        container.storyboardInitCompleted(ViewController.self) { r,c in
            c.articleListViewModel = r.resolve(ArticleListViewModeling.self)!
        }
        
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.makeKeyAndVisible()
        self.window = window
        
        let swinjectStoryboard = SwinjectStoryboard.create(name: "Main", bundle: nil, container: container)
        window.rootViewController = swinjectStoryboard.instantiateInitialViewController()

        return true
    }

    // MARK: UISceneSession Lifecycle


}

