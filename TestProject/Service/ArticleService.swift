//
//  ArticleService.swift
//  HorizontalList
//
//  Created by Raphael Rajaofetra on 2020-11-23.
//

import RxSwift

protocol ArticleServicing {
    func loadArticles() -> Observable<Response>
}

class ArticleService: ArticleServicing {
    private let network: Networking
    
    init(network: Networking) {
        self.network = network
    }
    
    func loadArticles() -> Observable<Response> {
        return network.getArticles()
    }
}
