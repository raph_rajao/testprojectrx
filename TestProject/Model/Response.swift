//
//  Response.swift
//  HorizontalList
//
//  Created by Raphael Rajaofetra on 2020-11-23.
//
import Himotoki

struct Response {
  let totalResults: Int
  let articles: [Article]
}

extension Response: Himotoki.Decodable {
    static func decode(_ e: Extractor) throws -> Response {
        return try Response(
            totalResults: e <| "totalResults",
            articles: e <| "articles"
        )
    }
}

