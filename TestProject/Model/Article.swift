//
//  Article.swift
//  HorizontalList
//
//  Created by Raphael Rajaofetra on 2020-11-23.
//
import Himotoki

struct Article {
  let image: String?
  let source: String?
  let title: String
  let description: String
}

extension Article: Himotoki.Decodable {
    static func decode(_ e: Extractor) throws -> Article {
        return try Article(
            image: e <|? "urlToImage",
            source: e <|? ["source", "name"],
            title: e <| "title",
            description: e <| "description"
        )
    }
}

