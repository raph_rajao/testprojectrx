//
//  Network.swift
//  HorizontalList
//
//  Created by Raphael Rajaofetra on 2020-11-23.
//

import RxSwift
import RxCocoa
import Alamofire
import AlamofireImage
import Himotoki

protocol Networking {
    func getArticles<D: Himotoki.Decodable>() -> Observable<D>
    func image(url: String) -> Observable<UIImage>
}

public enum NetworkError: Error {
    case Unknown
    case incorrectData
}

final class Network: Networking {
    private let queue = DispatchQueue(label: "HorizontalList.Network.Queue")

    func getArticles<D: Himotoki.Decodable>() -> Observable<D> {
        self.request(url: "https://newsapi.org/v2/everything?q=bitcoin&from=2020-11-23&sortBy=publishedAt&apiKey=a9b583a02d944727a45b209e6ebffb22", parameters: nil).map{
            do {
                return try D.decodeValue($0)
            } catch let error {
                print(error)
                throw error
            }
        }
    }
    
    func request(url: String, parameters: [String : Any]?) -> Observable<Any> {
        return Observable.create { observer in
            let request = AF.request(url, method: .get, parameters: parameters)
                .validate()
                .responseJSON(queue: self.queue) { response in
                    switch response.result {
                    case .success(let value):
                        observer.onNext(value)
                        observer.onCompleted()
                    case .failure(let _):
                        observer.onError(NetworkError.incorrectData)
                    }
                }
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func image(url: String) -> Observable<UIImage> {
        
        return Observable.create { observer in
            let request = AF.request(url)
                .validate()
                .responseData(queue: self.queue) { response in
                    print(response)
                    switch response.result {
                    case .success(let data):
                        guard let image = UIImage(data: data) else {
                            observer.onError(NetworkError.incorrectData)
                            return
                        }
                        observer.onNext(image)
                        observer.onCompleted()
                    case .failure(let error):
                        observer.onError(error)
                    }
                    
                }
            return Disposables.create {
                request.cancel()
            }
        }
    }
}
